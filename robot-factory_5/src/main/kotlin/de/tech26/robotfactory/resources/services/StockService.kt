package de.tech26.robotfactory.resources.services

import de.tech26.robotfactory.domain.entities.exceptions.InvalidCodeException
import de.tech26.robotfactory.domain.entities.exceptions.InvalidStockUpdateException
import de.tech26.robotfactory.domain.entities.Order
import de.tech26.robotfactory.domain.entities.enums.MaterialName
import de.tech26.robotfactory.domain.interfaces.Component
import de.tech26.robotfactory.domain.entities.Arms
import de.tech26.robotfactory.domain.entities.Face
import de.tech26.robotfactory.domain.entities.Material
import de.tech26.robotfactory.domain.entities.Mobility
import de.tech26.robotfactory.domain.entities.enums.ArmsName
import de.tech26.robotfactory.domain.entities.enums.FaceName
import de.tech26.robotfactory.domain.entities.enums.MobilityName
import java.math.BigDecimal
import java.math.BigInteger

class StockService {
    private val stock = hashMapOf<String, Component>()

    fun createStock(): HashMap<String, Component> {
        val lines = IOService().readAsText("stock.txt").split(";")

        lines.forEach { line ->

            if (!line.isNullOrBlank()) {
                var (code, price, available) = line.split(",")

                code = code.trim()
                when (code) {
                    in FaceName.getCodes() -> stock[code] = Face(code, price.toBigDecimal(), available.toBigInteger(), FaceName.fromCode(code))
                    in ArmsName.getCodes() -> stock[code] = Arms(code, price.toBigDecimal(), available.toBigInteger(), ArmsName.fromCode(code))
                    in MobilityName.getCodes() -> stock[code] = Mobility(code, price.toBigDecimal(), available.toBigInteger(), MobilityName.fromCode(code))
                    in MaterialName.getCodes() -> stock[code] = Material(code, price.toBigDecimal(), available.toBigInteger(), MaterialName.fromCode(code))

                    else -> throw InvalidCodeException()
                }

            }

        }

        return stock
    }

    fun validateOrder(components: ArrayList<String>?): Order {
        val order = ValidateOrderService().validateOrder(components, stock)
        order.total = updateStock(order)

        return order
    }

    fun getAvailability(code: String) = stock[code]?.available

    private fun updateStock(order: Order): BigDecimal {
        var total = BigDecimal.ZERO
        order.components.forEach { componentName ->
            val item = stock[componentName.toCode()]

            if (item != null && item.available > BigInteger.ZERO) {
                item.available = item.available.minus(BigInteger.ONE);
                stock[componentName.toCode()] = item
                total += item.price
            } else {
                throw InvalidStockUpdateException()
            }
        }
        return total
    }
}