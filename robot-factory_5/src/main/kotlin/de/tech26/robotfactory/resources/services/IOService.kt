package de.tech26.robotfactory.resources.services

class IOService {
    fun readAsText(fileName: String): String =
            javaClass.classLoader.getResource(fileName).readText(Charsets.UTF_8)
}