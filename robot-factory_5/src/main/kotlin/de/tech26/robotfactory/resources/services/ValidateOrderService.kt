package de.tech26.robotfactory.resources.services

import de.tech26.robotfactory.domain.entities.Order
import de.tech26.robotfactory.domain.entities.enums.ArmsName
import de.tech26.robotfactory.domain.entities.enums.FaceName
import de.tech26.robotfactory.domain.entities.enums.MaterialName
import de.tech26.robotfactory.domain.entities.enums.MobilityName
import de.tech26.robotfactory.domain.entities.exceptions.InsufficientStockException
import de.tech26.robotfactory.domain.entities.exceptions.InvalidCodeException
import de.tech26.robotfactory.domain.entities.exceptions.InvalidOrderException
import de.tech26.robotfactory.domain.interfaces.Component
import java.math.BigInteger

class ValidateOrderService {

    fun validateOrder(components: ArrayList<String>?, stock: HashMap<String, Component>): Order {
        val order = Order()

        components?.forEach { code ->
            if (stock[code]?.available == BigInteger.ZERO) {
                throw InsufficientStockException()
            }

            when {
                validFace(code,order) -> {
                    order.hasFace = true
                    order.components.add(FaceName.fromCode(code))
                }

                validArms(code,order) -> {
                    order.hasArms = true
                    order.components.add(ArmsName.fromCode(code))
                }

                validMobility(code,order) -> {
                    order.hasMobility = true
                    order.components.add(MobilityName.fromCode(code))
                }

                validMaterial(code,order) -> {
                    order.hasMaterial = true
                    order.components.add(MaterialName.fromCode(code))
                }

                else -> throw InvalidCodeException()
            }
        }

        if (!order.hasFace || !order.hasArms || !order.hasMobility || !order.hasMaterial) {
            throw InvalidOrderException()
        }

        return order
    }

    private fun validFace(code: String, order: Order): Boolean =
            code in FaceName.getCodes() && !order.hasFace

    private fun validArms(code: String, order: Order): Boolean =
            code in ArmsName.getCodes() && !order.hasArms

    private fun validMobility(code: String, order: Order): Boolean =
            code in MobilityName.getCodes() && !order.hasMobility

    private fun validMaterial(code: String, order: Order): Boolean =
            code in MaterialName.getCodes() && !order.hasMaterial
}