package de.tech26.robotfactory.web.dto

data class OrderRequest(
        val components: ArrayList<String>?
)