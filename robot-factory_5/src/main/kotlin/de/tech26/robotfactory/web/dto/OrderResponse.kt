package de.tech26.robotfactory.web.dto

import java.math.BigDecimal

data class OrderResponse(
        val order_id: String,
        val total: BigDecimal
)