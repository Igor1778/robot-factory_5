package de.tech26.robotfactory.domain.entities

import de.tech26.robotfactory.domain.entities.enums.MobilityName
import de.tech26.robotfactory.domain.interfaces.Component
import java.math.BigDecimal
import java.math.BigInteger

data class Mobility(
        override val code: String,
        override val price: BigDecimal,
        override var available: BigInteger,
        override val name: MobilityName
) : Component