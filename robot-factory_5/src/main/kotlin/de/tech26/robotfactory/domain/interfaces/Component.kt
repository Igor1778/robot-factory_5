package de.tech26.robotfactory.domain.interfaces

import java.math.BigDecimal
import java.math.BigInteger


interface Component {
    val code: String
    val price: BigDecimal
    var available: BigInteger
    val name: ComponentName
}