package de.tech26.robotfactory.domain.entities.enums

import de.tech26.robotfactory.domain.entities.exceptions.InvalidCodeException
import de.tech26.robotfactory.domain.interfaces.ComponentName

enum class MaterialName : ComponentName {
    Bioplastic, Metallic;

    override fun toCode(): String =
            when (this) {
                Bioplastic ->"I"
                Metallic ->"J"
            }

    companion object {
        fun fromCode(code: String): MaterialName =
                when (code) {
                    "I" -> Bioplastic
                    "J" -> Metallic
                    else -> throw InvalidCodeException()
                }

        fun getCodes(): List<String> = listOf("I", "J")
    }

}