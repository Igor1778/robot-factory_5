package de.tech26.robotfactory.domain.entities.exceptions

import java.lang.Exception

class InsufficientStockException() : Exception()