package de.tech26.robotfactory.domain.entities

import de.tech26.robotfactory.domain.interfaces.ComponentName
import java.math.BigDecimal
import java.util.UUID

class Order(
        val orderId: String = UUID.randomUUID().toString(),
        var hasFace: Boolean = false,
        var hasArms: Boolean = false,
        var hasMobility: Boolean = false,
        var hasMaterial: Boolean = false,
        val components: MutableList<ComponentName> = mutableListOf(),
        var total: BigDecimal = BigDecimal.ZERO
)