package de.tech26.robotfactory.domain.entities.enums

import de.tech26.robotfactory.domain.entities.exceptions.InvalidCodeException
import de.tech26.robotfactory.domain.interfaces.ComponentName

enum class MobilityName : ComponentName {
    Wheels, Legs, Tracks;

    override fun toCode(): String =
            when (this) {
                Wheels -> "F"
                Legs -> "G"
                Tracks -> "H"
            }

    companion object {
        fun fromCode(code: String): MobilityName =
                when (code) {
                    "F" -> Wheels
                    "G" -> Legs
                    "H" -> Tracks
                    else -> throw InvalidCodeException()
                }

        fun getCodes(): List<String> = listOf("F", "G", "H")
    }

}