package de.tech26.robotfactory.domain.interfaces

interface ComponentName {
    fun toCode(): String
}