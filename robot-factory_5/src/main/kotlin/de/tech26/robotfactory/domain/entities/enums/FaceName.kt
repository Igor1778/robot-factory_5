package de.tech26.robotfactory.domain.entities.enums

import de.tech26.robotfactory.domain.entities.exceptions.InvalidCodeException
import de.tech26.robotfactory.domain.interfaces.ComponentName

enum class FaceName : ComponentName {
    Humanoid, LCD, Steampunk;

    override fun toCode(): String =
            when (this) {
                Humanoid -> "A"
                LCD -> "B"
                Steampunk -> "C"
            }

    companion object {
        fun fromCode(code: String): FaceName =
                when (code) {
                    "A" -> Humanoid
                    "B" -> LCD
                    "C" -> Steampunk
                    else -> throw InvalidCodeException()
                }


        fun getCodes(): List<String> = listOf("A", "B", "C")
    }

}