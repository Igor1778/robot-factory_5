package de.tech26.robotfactory.domain.entities


import de.tech26.robotfactory.domain.entities.enums.FaceName
import de.tech26.robotfactory.domain.interfaces.Component
import java.math.BigDecimal
import java.math.BigInteger

data class Face(
        override val code: String,
        override val price: BigDecimal,
        override var available: BigInteger,
        override val name: FaceName
) : Component