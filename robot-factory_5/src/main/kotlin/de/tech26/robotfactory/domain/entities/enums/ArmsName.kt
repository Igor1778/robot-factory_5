package de.tech26.robotfactory.domain.entities.enums

import de.tech26.robotfactory.domain.entities.exceptions.InvalidCodeException
import de.tech26.robotfactory.domain.interfaces.ComponentName

enum class ArmsName : ComponentName {
    Hands, Grippers;

    override fun toCode(): String =
            when (this) {
                Hands -> "D"
                Grippers -> "E"
            }

    companion object {
        fun fromCode(code: String): ArmsName =
                when (code) {
                    "D" -> Hands
                    "E" -> Grippers
                    else -> throw InvalidCodeException()
                }

        fun getCodes(): List<String> = listOf("D", "E")
    }

}