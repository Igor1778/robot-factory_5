package de.tech26.robotfactory

import de.tech26.robotfactory.domain.entities.exceptions.InsufficientStockException
import de.tech26.robotfactory.domain.entities.exceptions.InvalidCodeException
import de.tech26.robotfactory.domain.entities.exceptions.InvalidOrderException
import de.tech26.robotfactory.resources.services.StockService
import de.tech26.robotfactory.web.dto.OrderRequest
import de.tech26.robotfactory.web.dto.OrderResponse
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class MessageResource {
    val stock = StockService()
    val stockItems = stock.createStock()

    @GetMapping("/")
    fun index() : String {
        return "OK: version 0.0.1"
    }

    @PostMapping("/orders")
    fun orders(@RequestBody orderRequest: OrderRequest): ResponseEntity<Any> {

        return try {
            val order = stock.validateOrder(orderRequest.components)
            ResponseEntity(OrderResponse(order_id = order.orderId, total = order.total),HttpStatus.CREATED)
        } catch (e: InvalidOrderException){
            ResponseEntity(hashMapOf("message" to "the order is invalid"), HttpStatus.UNPROCESSABLE_ENTITY)
        } catch (e: InsufficientStockException){
            ResponseEntity(hashMapOf("message" to "out of stock"), HttpStatus.UNPROCESSABLE_ENTITY)
        } catch (e: InvalidCodeException){
            ResponseEntity(hashMapOf("message" to "the order has invalid code"), HttpStatus.UNPROCESSABLE_ENTITY)
        }
    }
}
