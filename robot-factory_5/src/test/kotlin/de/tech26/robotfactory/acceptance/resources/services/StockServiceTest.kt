package de.tech26.robotfactory.acceptance.resources.services

import de.tech26.robotfactory.domain.entities.exceptions.InsufficientStockException
import de.tech26.robotfactory.domain.entities.exceptions.InvalidCodeException
import de.tech26.robotfactory.domain.entities.exceptions.InvalidOrderException
import de.tech26.robotfactory.resources.services.StockService
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.boot.test.context.SpringBootTest
import java.math.BigInteger

@SpringBootTest
class StockServiceTest {

    private val stockService = StockService()
    private val initialStockA = 9.toBigInteger()
    private val initialStockD = 1.toBigInteger()
    private val initialStockF = 2.toBigInteger()
    private val initialStockI = 92.toBigInteger()

    @Test
    fun `should initialize the stock`() {
        stockService.createStock()

        Assertions.assertEquals(initialStockA, stockService.getAvailability("A"))
        Assertions.assertEquals(initialStockD, stockService.getAvailability("D"))
        Assertions.assertEquals(initialStockF, stockService.getAvailability("F"))
        Assertions.assertEquals(initialStockI, stockService.getAvailability("I"))
    }

    @Test
    fun `should update stock when order is valid`() {
        stockService.createStock()
        stockService.validateOrder(arrayListOf("A", "D", "F", "I"))

        Assertions.assertEquals(initialStockA.minus(BigInteger.ONE), stockService.getAvailability("A"))
        Assertions.assertEquals(initialStockD.minus(BigInteger.ONE), stockService.getAvailability("D"))
        Assertions.assertEquals(initialStockF.minus(BigInteger.ONE), stockService.getAvailability("F"))
        Assertions.assertEquals(initialStockI.minus(BigInteger.ONE), stockService.getAvailability("I"))
    }

    @Test
    fun `should return error when order has missing components`() {
        stockService.createStock()
        val missingComponentOrder = arrayListOf("D", "F", "I")

        assertThrows<InvalidOrderException> {
            stockService.validateOrder(missingComponentOrder)
        }
    }

    @Test
    fun `should return error when order has more than one unit of the same component type`() {
        stockService.createStock()
        val sameComponentOrder = arrayListOf("A", "A", "D", "F", "I")

        assertThrows<InvalidCodeException> {
            stockService.validateOrder(sameComponentOrder)
        }
    }

    @Test
    fun `should return error when out of stock for the order`() {
        stockService.createStock()
        val outOfStockCode = "C"

        Assertions.assertEquals(BigInteger.ZERO, stockService.getAvailability(outOfStockCode))

        assertThrows<InsufficientStockException> {
            stockService.validateOrder(arrayListOf(outOfStockCode, "D", "F", "I"))
        }
    }

    @Test
    fun `should return error when order has unknown component`() {
        stockService.createStock()
        val unknownCode = "X"

        assertThrows<InvalidCodeException> {
            stockService.validateOrder(arrayListOf(unknownCode, "D", "F", "I"))
        }
    }
}