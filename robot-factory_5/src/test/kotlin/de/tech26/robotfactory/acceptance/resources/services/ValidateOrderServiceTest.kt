package de.tech26.robotfactory.acceptance.resources.services

import de.tech26.robotfactory.domain.entities.Arms
import de.tech26.robotfactory.domain.entities.Face
import de.tech26.robotfactory.domain.entities.Material
import de.tech26.robotfactory.domain.entities.Mobility
import de.tech26.robotfactory.domain.entities.enums.ArmsName
import de.tech26.robotfactory.domain.entities.enums.FaceName
import de.tech26.robotfactory.domain.entities.enums.MaterialName
import de.tech26.robotfactory.domain.entities.enums.MobilityName
import de.tech26.robotfactory.domain.entities.exceptions.InsufficientStockException
import de.tech26.robotfactory.domain.entities.exceptions.InvalidCodeException
import de.tech26.robotfactory.domain.entities.exceptions.InvalidOrderException
import de.tech26.robotfactory.domain.interfaces.Component
import de.tech26.robotfactory.resources.services.IOService
import de.tech26.robotfactory.resources.services.ValidateOrderService
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.boot.test.context.SpringBootTest
import java.math.BigInteger

@SpringBootTest
class ValidateOrderServiceTest {

    private val stock = createStockHelper()
    private val validateOrderService = ValidateOrderService()

    @Test
    fun `should update the order when order is valid`() {
        val expectedComponents = listOf(FaceName.fromCode("A"), ArmsName.fromCode("D"), MobilityName.fromCode("F"), MaterialName.fromCode("I"))
        val order = validateOrderService.validateOrder(arrayListOf("A", "D", "F", "I"), stock)

        Assertions.assertEquals(true, order.hasFace)
        Assertions.assertEquals(true, order.hasArms)
        Assertions.assertEquals(true, order.hasMobility)
        Assertions.assertEquals(true, order.hasMaterial)
        Assertions.assertEquals(expectedComponents, order.components)
    }

    @Test
    fun `should return error when order has missing components`() {
        val missingComponentOrder = arrayListOf("D", "F", "I")

        assertThrows<InvalidOrderException> {
            validateOrderService.validateOrder(missingComponentOrder, stock)
        }
    }

    @Test
    fun `should return error when order has more than one unit of the same component type`() {
        val sameComponentOrder = arrayListOf("A", "A", "D", "F", "I")

        assertThrows<InvalidCodeException> {
            validateOrderService.validateOrder(sameComponentOrder, stock)
        }
    }

    @Test
    fun `should return error when out of stock for the order`() {
        val outOfStockCode = "C"

        Assertions.assertEquals(BigInteger.ZERO, stock[outOfStockCode]?.available)

        assertThrows<InsufficientStockException> {
            validateOrderService.validateOrder(arrayListOf(outOfStockCode, "D", "F", "I"), stock)
        }
    }

    @Test
    fun `should return error when order has unknown component`() {
        val unknownCode = "X"

        assertThrows<InvalidCodeException> {
            validateOrderService.validateOrder(arrayListOf(unknownCode, "D", "F", "I"), stock)
        }
    }

    private fun createStockHelper(): HashMap<String, Component> {
        val stock = hashMapOf<String, Component>()

        val lines = IOService().readAsText("stock.txt").split(";")

        lines.forEach { line ->

            if (!line.isNullOrBlank()) {
                var (code, price, available) = line.split(",")

                code = code.trim()
                when (code) {
                    in FaceName.getCodes() -> stock[code] = Face(code, price.toBigDecimal(), available.toBigInteger(), FaceName.fromCode(code))
                    in ArmsName.getCodes() -> stock[code] = Arms(code, price.toBigDecimal(), available.toBigInteger(), ArmsName.fromCode(code))
                    in MobilityName.getCodes() -> stock[code] = Mobility(code, price.toBigDecimal(), available.toBigInteger(), MobilityName.fromCode(code))
                    in MaterialName.getCodes() -> stock[code] = Material(code, price.toBigDecimal(), available.toBigInteger(), MaterialName.fromCode(code))

                    else -> throw InvalidCodeException()
                }

            }

        }

        return stock
    }
}