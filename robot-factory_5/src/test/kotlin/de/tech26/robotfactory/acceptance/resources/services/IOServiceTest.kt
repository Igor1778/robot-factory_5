package de.tech26.robotfactory.acceptance.resources.services

import de.tech26.robotfactory.resources.services.IOService
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class IOServiceTest {

    @Test
    fun `should read a file and return the content`() {
        val expectedContent = """
            A,10.28,9,Face,Humanoid;
            B,24.07,7,Face,LCD;
        """.trimIndent()

        val content = IOService().readAsText("stock_mock.txt")

        Assertions.assertEquals(expectedContent, content)
    }
}